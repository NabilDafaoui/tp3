package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.TeacherService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {

    @MockBean
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {

        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
        System.out.println("_______________________");
        System.out.println(result.hashCode());
        System.out.println("_______________________");

    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        long id = 10;
        assumingThat(teacherService.getTeacher(id).isEmpty(),()->{
            assertTrue(teacherService.getTeacher(id).isEmpty());
            System.out.println("conditionner la suite du test");
        });
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                        .param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        assertNotNull(teacherRepository.findByLastName("Kermarrec"));
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws  Exception{

        long id = teacherRepository.findByLastName("Kermarrec").getId();
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Nabil")
                        .param("lastName", "Kermarrec")
                        .param("id", id+"")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        assertEquals("Nabil",teacherRepository.findByLastName("Kermarrec").getFirstName());
    }

}
